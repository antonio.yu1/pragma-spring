package co.com.pragma.spring.customer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class BirthPlaceDTO {

    private Long id;
    private String name;
}
