package co.com.pragma.spring.customer.controller;

import co.com.pragma.spring.customer.exception.CustomerRequestException;
import co.com.pragma.spring.customer.model.CustomerDTO;
import co.com.pragma.spring.customer.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping(value = "/customers")
@Validated
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @ApiOperation(value = "Returns a customer given an identification type and an identification.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Customer found."),
            @ApiResponse(code = 404, message = "Customer not found.")})
    @GetMapping(value = "/{identificationType}/{identification}")
    public ResponseEntity<CustomerDTO> getCustomer(@PathVariable("identificationType") Long identificationType,
                                                   @PathVariable("identification") String identification) {
        ResponseEntity<CustomerDTO> response;
        CustomerDTO customerDTO = customerService.getCustomer(identificationType, identification);
        if(customerDTO != null) {
            response = new ResponseEntity<>(customerDTO, HttpStatus.OK);
        } else {
            throw new CustomerRequestException("Customer not found.", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @ApiOperation(value = "Returns a list of customers older than the given age.")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Customers found.") })
    @GetMapping(params = "age")
    public ResponseEntity<List<CustomerDTO>> filterCustomersByAge(@RequestParam("age") Integer age) {
        ResponseEntity<List<CustomerDTO>> response;
        List<CustomerDTO> listCustomerDTO = customerService.filterCustomersByAge(age);
        response = new ResponseEntity<>(listCustomerDTO, HttpStatus.OK);
        return response;
    }

    @ApiOperation(value = "Create a customer.")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Customer created."),
            @ApiResponse(code = 409, message = "Customer already exists."),
            @ApiResponse(code = 422, message = "One or more fields are invalid.")})
    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public ResponseEntity<String> createCustomer(@Valid @RequestBody CustomerDTO customer) {
        ResponseEntity<String> response;
        boolean created = customerService.createCustomer(customer);
        if(created) {
            response = new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            throw new CustomerRequestException("Customer already exists.", HttpStatus.CONFLICT);
        }
        return response;
    }

    @ApiOperation(value = "Update a customer's firstname, lastname, age, photo or birthplace.")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Customer updated."),
            @ApiResponse(code = 404, message = "Customer not found."),
            @ApiResponse(code = 422, message = "One or more fields are invalid.")})
    @PutMapping
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> updateCustomer(@Valid @RequestBody CustomerDTO customer) {
        ResponseEntity<Void> response;
        boolean updated = customerService.updateCustomer(customer);
        if(updated) {
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new CustomerRequestException("Customer not found.", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @ApiOperation(value = "Delete a customer.")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Customer deleted."),
            @ApiResponse(code = 404, message = "Customer not found.")})
    @DeleteMapping(value = "/{identificationType}/{identification}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public ResponseEntity<Void> deleteCustomer(@PathVariable("identificationType") Long identificationType,
                                                 @PathVariable("identification") String identification) {
        ResponseEntity<Void> response;
        boolean deleted = customerService.deleteCustomer(identificationType, identification);
        if(deleted) {
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new CustomerRequestException("Customer not found.", HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
