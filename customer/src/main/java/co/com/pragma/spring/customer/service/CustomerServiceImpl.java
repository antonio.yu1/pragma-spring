package co.com.pragma.spring.customer.service;

import co.com.pragma.spring.customer.client.PhotoClient;
import co.com.pragma.spring.customer.entity.BirthPlace;
import co.com.pragma.spring.customer.entity.Customer;
import co.com.pragma.spring.customer.entity.IdentificationType;
import co.com.pragma.spring.customer.model.CustomerDTO;
import co.com.pragma.spring.customer.model.PhotoDTO;
import co.com.pragma.spring.customer.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Qualifier("defaultCustomerService")
public class CustomerServiceImpl implements CustomerService {
// TODO: Finish implementation

    private final CustomerRepository customerRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    private final PhotoClient photoClient;

    @Override
    public CustomerDTO getCustomer(Long identificationType, String identification){
        CustomerDTO customerDTO = null;
        Customer customer = customerRepository.findByIdentificationAndIdentificationType(
                identification,
                IdentificationType.builder().id(identificationType).build());
        if(customer != null) {
            customerDTO = modelMapper.map(customer, CustomerDTO.class);
            if(customerDTO.getPhoto() != null && !customerDTO.getPhoto().getId().isEmpty()) {
                ResponseEntity<PhotoDTO> response = photoClient.getPhoto(customerDTO.getPhoto().getId());
                PhotoDTO photoDTO = modelMapper.map(response.getBody(), PhotoDTO.class);
                customerDTO.setPhoto(photoDTO);
            }
        }
        return customerDTO;
    }

    @Override
    public List<CustomerDTO> filterCustomersByAge(Integer age) {
        List<CustomerDTO> listCustomerDTO = new ArrayList<>();
        Map<String, CustomerDTO> customersDTOMap = new HashMap<>();
        for(Customer customer : customerRepository.findByAgeGreaterThanEqual(age)) {
            if(customer.getPhotoId() != null) {
                customersDTOMap.put(customer.getPhotoId(), modelMapper.map(customer, CustomerDTO.class));
            } else {
                listCustomerDTO.add(modelMapper.map(customer, CustomerDTO.class));
            }
        }
        // TODO: Wrap this functionality into another method to uphold single responsibility principle.
        ResponseEntity<List<PhotoDTO>> response = photoClient.getPhotos(new ArrayList<>(customersDTOMap.keySet()));
        if(response.getBody() != null && !response.getBody().isEmpty())  {
            for(PhotoDTO photoDTO : response.getBody()) {
                customersDTOMap.get(photoDTO.getId()).setPhoto(photoDTO);
                listCustomerDTO.add(customersDTOMap.get(photoDTO.getId()));
            }
        }
        return listCustomerDTO;
    }

    @Transactional
    @Override
    public boolean createCustomer(CustomerDTO customerDTO) {
        int created = 0;
        Customer customer = customerRepository.findByIdentificationAndIdentificationType(
                customerDTO.getIdentification(),
                IdentificationType.builder().id(customerDTO.getIdentificationType().getId()).build());
        if(customer == null) {
            Instant creation = Instant.now();
            customer = modelMapper.map(customerDTO, Customer.class);
            customer.setCreated(creation);
            customer.setUpdated(creation);
            if(customerDTO.getPhoto() != null) {
                ResponseEntity<PhotoDTO> response = photoClient.createPhoto(customerDTO.getPhoto());
                PhotoDTO photo = modelMapper.map(response.getBody(), PhotoDTO.class);
                customer.setPhotoId(photo.getId());
            }
            customerRepository.save(customer);
            created = 1;
        }
        return created > 0;
    }

    @Transactional
    @Override
    public boolean updateCustomer(CustomerDTO customerDTO) {
        int updated = 0;
        Customer customer = customerRepository.findByIdentificationAndIdentificationType(
                customerDTO.getIdentification(),
                IdentificationType.builder().id(customerDTO.getIdentificationType().getId()).build());
        if(customer != null) {
            customer.setFirstname(customerDTO.getFirstname());
            customer.setLastname(customerDTO.getLastname());
            customer.setAge(customerDTO.getAge());
            customer.setBirthPlace(modelMapper.map(customerDTO.getBirthPlace(), BirthPlace.class));
            customer.setUpdated(Instant.now());
            if(customerDTO.getPhoto() != null) {
                if(customer.getPhotoId() == null) {
                    ResponseEntity<PhotoDTO> response = photoClient.createPhoto(customerDTO.getPhoto());
                    PhotoDTO photo = modelMapper.map(response.getBody(), PhotoDTO.class);
                    customer.setPhotoId(photo.getId());
                } else { photoClient.updatePhoto(customerDTO.getPhoto()); }
            }
            customerRepository.save(customer);
            updated = 1;
        }
        return updated > 0;
    }

    @Transactional
    @Override
    public boolean deleteCustomer(Long identificationType, String identification) {
        int deleted = 0;
        Customer customer = customerRepository.findByIdentificationAndIdentificationType(
                identification,
                IdentificationType.builder().id(identificationType).build());
        if(customer != null) {
            String photoId = customer.getPhotoId();
            if(photoId != null && !photoId.isEmpty()) {
                photoClient.deletePhoto(photoId);
            }
            customerRepository.delete(customer);
            deleted = 1;
        }
        return deleted > 0;
    }
}
