package co.com.pragma.spring.customer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class PhotoDTO implements Serializable {
    private String id;
    private byte[] imageData;
}
