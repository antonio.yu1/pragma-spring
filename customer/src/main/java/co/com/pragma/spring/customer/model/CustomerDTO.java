package co.com.pragma.spring.customer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.io.Serializable;

@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class CustomerDTO implements Serializable {

    @NotBlank(message = "This field cannot be empty.")
    private String firstname;

    @NotBlank(message = "This field cannot be empty.")
    private String lastname;

    @NotNull(message = "This field cannot be not null.")
    @Positive(message = "This field cannot be negative.")
    private Integer age;

    @NotBlank(message = "This field cannot be null.")
    private String identification;

    @NotNull(message = "This field cannot be null.")
    private IdentificationTypeDTO identificationType;

    @NotNull(message = "This field cannot be null.")
    private BirthPlaceDTO birthPlace;

    private PhotoDTO photo;
}
