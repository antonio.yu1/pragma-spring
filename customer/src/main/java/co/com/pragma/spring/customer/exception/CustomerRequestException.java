package co.com.pragma.spring.customer.exception;

import org.springframework.http.HttpStatus;

public class CustomerRequestException extends RuntimeException{

    private final HttpStatus httpStatus;

    public CustomerRequestException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
