package co.com.pragma.spring.customer.service;

import co.com.pragma.spring.customer.model.CustomerDTO;

import java.util.List;

public interface CustomerService {

    CustomerDTO getCustomer(Long identificationType, String identification);
    List<CustomerDTO> filterCustomersByAge(Integer age);
    boolean createCustomer(CustomerDTO customer);
    boolean updateCustomer(CustomerDTO customer);
    boolean deleteCustomer(Long identificationType, String identification);
}
