package co.com.pragma.spring.customer.client;

import co.com.pragma.spring.customer.model.PhotoDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "photo-service", fallback = PhotoClientHystrixFallback.class)
public interface PhotoClient {

    @GetMapping(value = "/{id}")
    ResponseEntity<PhotoDTO> getPhoto(@PathVariable("id") String id);

    @GetMapping(value = "/photos", params = "ids")
    ResponseEntity<List<PhotoDTO>> getPhotos(@RequestParam("ids") List<String> id);

    @PostMapping(value = "/photos")
    ResponseEntity<PhotoDTO> createPhoto(@RequestBody PhotoDTO photoDTO);

    @PutMapping(value = "/photos")
    void updatePhoto(@RequestBody PhotoDTO photoDTO);

    @DeleteMapping(value = "/photos/{id}")
    void deletePhoto(@PathVariable("id") String id);
}
