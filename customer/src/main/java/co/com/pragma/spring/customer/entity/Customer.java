package co.com.pragma.spring.customer.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "customer")
@Data
@AllArgsConstructor @NoArgsConstructor @Builder
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstname;

    private String lastname;

    private Integer age;

    private String identification;

    private String photoId;

    private Instant created;

    private Instant updated;

    @ManyToOne
    @JoinColumn(name = "identification_type_id", nullable = false)
    private IdentificationType identificationType;

    @ManyToOne
    @JoinColumn(name = "birthplace_id", nullable = false)
    private BirthPlace birthPlace;

}
