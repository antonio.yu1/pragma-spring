package co.com.pragma.spring.customer.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class CustomerExceptionHandler {

    @ExceptionHandler(value = {CustomerRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(CustomerRequestException e) {
        CustomerException customerException = CustomerException.builder()
                .message(e.getMessage())
                .httpStatus(e.getHttpStatus())
                .timestamp(ZonedDateTime.now(ZoneId.of("Z")))
                .build();
        return new ResponseEntity<>(customerException, e.getHttpStatus());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleValidationError(MethodArgumentNotValidException e) {
        HttpStatus httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
        CustomerException customerException = CustomerException.builder()
                .message("One or more fields are invalid.")
                .httpStatus(httpStatus)
                .timestamp(ZonedDateTime.now(ZoneId.of("Z")))
                .build();
        return new ResponseEntity<>(customerException, httpStatus);
    }
}
