package co.com.pragma.spring.customer.repository;

import co.com.pragma.spring.customer.entity.Customer;
import co.com.pragma.spring.customer.entity.IdentificationType;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("defaultCustomerRepository")
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findByIdentificationAndIdentificationType(String identification,
                                                       IdentificationType identificationType);
    List<Customer> findByAgeGreaterThanEqual(Integer age);
}
