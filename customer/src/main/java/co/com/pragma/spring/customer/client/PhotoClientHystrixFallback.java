package co.com.pragma.spring.customer.client;

import co.com.pragma.spring.customer.model.PhotoDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PhotoClientHystrixFallback implements PhotoClient {

    @Override
    public ResponseEntity<PhotoDTO> getPhoto(String id) {
        PhotoDTO photoDTO = PhotoDTO.builder()
                .id("")
                .imageData(null)
                .build();
        return new ResponseEntity<>(photoDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<PhotoDTO>> getPhotos(List<String> id) {
        List<PhotoDTO> listPhotoDTO = new ArrayList<>();
        return new ResponseEntity<>(listPhotoDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<PhotoDTO> createPhoto(PhotoDTO photoDTO) {
        return null;
    }

    @Override
    public void updatePhoto(PhotoDTO photoDTO) {
    }

    @Override
    public void deletePhoto(String id) {
    }
}
