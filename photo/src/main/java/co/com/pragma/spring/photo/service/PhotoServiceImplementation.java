package co.com.pragma.spring.photo.service;

import co.com.pragma.spring.photo.entity.Photo;
import co.com.pragma.spring.photo.model.PhotoDTO;
import co.com.pragma.spring.photo.repository.PhotoRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Qualifier("defaultPhotoService")
public class PhotoServiceImplementation implements PhotoService {

    private final PhotoRepository photoRepository;

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    public PhotoDTO getPhoto(String id) {
        PhotoDTO photoDTO = null;
        Optional<Photo> photo = photoRepository.findById(id);
        if(photo.isPresent()) {
            photoDTO = modelMapper.map(photo.get(), PhotoDTO.class);
        }
        return photoDTO;
    }

    @Override
    public List<PhotoDTO> getPhotos(List<String> listId) {
        Iterable<Photo> listPhoto = photoRepository.findAllById(listId);
        List<PhotoDTO> listPhotoDTO = new ArrayList<>();
        for (Photo photo : listPhoto) {
            PhotoDTO photoDTO = modelMapper.map(photo, PhotoDTO.class);
            listPhotoDTO.add(photoDTO);
        }
        return listPhotoDTO;
    }

    @Transactional
    @Override
    public PhotoDTO createPhoto(PhotoDTO photoDTO) {
        Instant creation = Instant.now();
        Photo photo = modelMapper.map(photoDTO, Photo.class);
        photo.setCreated(creation);
        photo.setUpdated(creation);
        photo = photoRepository.save(photo);
        photoDTO.setId(photo.getId());
        return photoDTO;
    }

    @Transactional
    @Override
    public boolean updatePhoto(PhotoDTO photoDTO) {
        int updated = 0;
        Optional<Photo> optionalPhoto = photoRepository.findById(photoDTO.getId());
        if(optionalPhoto.isPresent()) {
            Photo photo = optionalPhoto.get();
            photo.setImageData(photoDTO.getImageData());
            photo.setUpdated(Instant.now());
            photoRepository.save(photo);
            updated = 1;
        }
        return updated > 0;
    }

    @Transactional
    @Override
    public boolean deletePhoto(String id) {
        int deleted = 0;
        Optional<Photo> optionalPhoto = photoRepository.findById(id);
        if(optionalPhoto.isPresent()) {
            photoRepository.delete(optionalPhoto.get());
            deleted = 1;
        }
        return deleted > 0;
    }
}
