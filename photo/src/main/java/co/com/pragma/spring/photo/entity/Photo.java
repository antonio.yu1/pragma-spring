package co.com.pragma.spring.photo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Document
@AllArgsConstructor @NoArgsConstructor @Builder
public class Photo {

    @Id
    private String id;

    private Instant created;
    private Instant updated;

    private byte[] imageData;
}
