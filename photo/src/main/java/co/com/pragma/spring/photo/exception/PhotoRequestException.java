package co.com.pragma.spring.photo.exception;

import org.springframework.http.HttpStatus;

public class PhotoRequestException extends RuntimeException {

    private final HttpStatus httpStatus;

    public PhotoRequestException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
