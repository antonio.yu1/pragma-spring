package co.com.pragma.spring.photo.repository;

import co.com.pragma.spring.photo.entity.Photo;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Qualifier("defaultPhotoRepository")
public interface PhotoRepository extends MongoRepository<Photo, String> {
}
