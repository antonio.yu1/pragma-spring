package co.com.pragma.spring.photo.controller;


import co.com.pragma.spring.photo.exception.PhotoRequestException;
import co.com.pragma.spring.photo.model.PhotoDTO;
import co.com.pragma.spring.photo.service.PhotoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PUT})
@RequestMapping(value = "/photos")
public class PhotoController {

    @Autowired
    private PhotoService photoService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<PhotoDTO> getPhoto(@PathVariable("id") String id) {
        ResponseEntity<PhotoDTO> response;
        PhotoDTO photoDTO = photoService.getPhoto(id);
        if(photoDTO != null) {
            response = new ResponseEntity<>(photoDTO, HttpStatus.OK);
        } else {
            throw new PhotoRequestException("Photo not found.", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @GetMapping(params = "ids")
    public ResponseEntity<List<PhotoDTO>> getPhotos(@RequestParam("ids") List<String> id) {
        ResponseEntity<List<PhotoDTO>> response;
        List<PhotoDTO> listPhotoDTO = photoService.getPhotos(id);
        if(!listPhotoDTO.isEmpty()) {
            response = new ResponseEntity<>(listPhotoDTO, HttpStatus.OK);
        } else {
            throw new PhotoRequestException("Photos not found.", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @PostMapping
    public ResponseEntity<PhotoDTO> createPhoto(@RequestBody PhotoDTO photoDTO) {
        photoDTO = photoService.createPhoto(photoDTO);
        return new ResponseEntity<>(photoDTO, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<Void> updatePhoto(@RequestBody PhotoDTO photoDTO) {
        ResponseEntity<Void> response;
        boolean updated = photoService.updatePhoto(photoDTO);
        if(updated) {
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new PhotoRequestException("Photo not found.", HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> deletePhoto(@PathVariable("id") String id) {
        ResponseEntity<Void> response;
        boolean deleted = photoService.deletePhoto(id);
        if(deleted) {
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            throw new PhotoRequestException("Photo not found.", HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
