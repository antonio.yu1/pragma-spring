package co.com.pragma.spring.photo.service;

import co.com.pragma.spring.photo.model.PhotoDTO;

import java.util.List;

public interface PhotoService {
    PhotoDTO getPhoto(String id);
    List<PhotoDTO> getPhotos(List<String> listId);
    PhotoDTO createPhoto(PhotoDTO photoDTO);
    boolean updatePhoto(PhotoDTO photoDTO);
    boolean deletePhoto(String id);
}
