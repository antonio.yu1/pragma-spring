package co.com.pragma.spring.photo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

@ControllerAdvice
public class PhotoRequestExceptionHandler {

    @ExceptionHandler(value = {PhotoRequestException.class})
    public ResponseEntity<Object> handleApiRequestException(PhotoRequestException e) {
        PhotoException customerException = PhotoException.builder()
                .message(e.getMessage())
                .httpStatus(e.getHttpStatus())
                .timestamp(ZonedDateTime.now(ZoneId.of("Z")))
                .build();
        return new ResponseEntity<>(customerException, e.getHttpStatus());
    }
}